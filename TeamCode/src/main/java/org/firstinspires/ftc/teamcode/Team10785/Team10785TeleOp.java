package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp
public class Team10785TeleOp extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public Vision visionSystem;
    public String found;
    public int target;
    public int liftCount;
    public int startingArmPos=0;
    @Override
    public void runOpMode(){
        robot.init(hardwareMap);
        // = new Vision();

        //visionSystem.initVuforia(hardwareMap);
        //visionSystem.initTfod(hardwareMap);


        telemetry.addData("Stauts","Initialized 1");
        telemetry.update();

        waitForStart();
        //visionSystem.activateTFOD();

        startingArmPos=robot.arm1.getCurrentPosition();
        target=startingArmPos;
        robot.arm1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.arm2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
       // robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
       // robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        target = startingArmPos;
        liftCount = 0;

        while(opModeIsActive()){

            // Convert joysticks to desired motion.
            Mecanum.Motion motion = Mecanum.joystickToMotion(
                    gamepad1.left_stick_x, gamepad1.left_stick_y,
                    gamepad1.right_stick_x, gamepad1.right_stick_y);

            // Convert desired motion to wheel powers, with power clamping.
            motion.vTheta= motion.vTheta * 0.8;
            Mecanum.Wheels wheels = Mecanum.motionToWheels(motion);
            robot.leftFrontMotor.setPower(wheels.frontLeft);
            robot.rightFrontMotor.setPower(wheels.frontRight);
            robot.leftBackMotor.setPower(wheels.backLeft);
            robot.rightBackMotor.setPower(wheels.backRight);


            if (gamepad2.dpad_down ) {
                robot.lift2.setPower(-1);
                robot.lift.setPower(1);
            }else if (gamepad2.dpad_up) {
                robot.lift2.setPower(1);
                robot.lift.setPower(-1);
            }else{
                robot.lift2.setPower(0);
                robot.lift.setPower(0);
            }


            //lift
            /*robot.lift.setPower(1.0);
            if (gamepad1.left_stick_y > .5) {
                liftCount = liftCount + 10;
                if (liftCount > 7999)
                    liftCount = 7999;}
            if (gamepad1.left_stick_y < -.5) {
                liftCount = liftCount - 10;
                if (liftCount > 0)
                    liftCount = 0;} */
           // robot.lift2.setPower(gamepad2.left_trigger-gamepad2.right_trigger);
            //lift
            /*robot.lift.setPower(1.0);
            if (gamepad1.left_stick_y > .5) {
                liftCount = liftCount + 10;
                if (liftCount > 7999)
                    liftCount = 7999;}
            if (gamepad1.left_stick_y < -.5) {
                liftCount = liftCount - 10;
                if (liftCount > 0)
                    liftCount = 0;}
*/

            //Collectors
            //robot.collector1.set;

            //if (visionSystem.updatePosition())
            //    found="Target Visible";
            //else
            //    found="Target Not Found";

            //visionSystem.updateMineralPostion();

            if (gamepad2.dpad_right) {
                target = target - 20;
                if (target < 0)
                    target = 0;
            }
            if (gamepad2.dpad_left) {
                target = target + 20;
                if (target > 2196*4)
                    target = 2196*4;
            }

            if (gamepad2.b) {
                target = 2562;   //1055*4
            }

            if (gamepad2.x) {
                target = 2196*4;   //original: 4392
            }
            if (gamepad2.y) {
                target = 4665;   //original: 1000
            }
           /* if (gamepad2.right_bumper) {
                robot.collector1.setPower(-1);
            } else {
                robot.collector1.setPower(gamepad2.right_trigger);
            }
            if (gamepad2.left_bumper) {
                robot.collector2.setPower(1);
            } else {
                robot.collector2.setPower(-gamepad2.left_trigger);
            }
            if (gamepad2.left_bumper) {
                robot.collector2.setPower(1);
            } else {
                robot.collector2.setPower(-gamepad2.left_trigger);
            }*/

            robot.collector2.setPower(gamepad2.left_stick_y);
            robot.collector1.setPower(-gamepad2.right_stick_y);

            if (gamepad1.left_bumper) {
                robot.extend1.setPower(-1);
                robot.extend2.setPower(1);
            } else {
                robot.extend1.setPower(0);
                robot.extend2.setPower(0);
            }

            if (gamepad1.right_bumper) {
                robot.extend1.setPower(1);
                robot.extend2.setPower(-1);
            } else {
                robot.extend1.setPower(0);
                robot.extend2.setPower(0);
            }



            robot.arm1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.arm2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.arm1.setTargetPosition(target);
            robot.arm2.setTargetPosition(target);
            robot.arm1.setPower(.50);
            robot.arm2.setPower(.50);

            /*robot.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.lift.setTargetPosition(liftCount);
            robot.lift.setPower(1);*/
            /*robot.lift2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.lift2.setTargetPosition(liftCount);
            robot.lift2.setPower(1);*/



            telemetry.addData("Status","Running");
            telemetry.addData("MoveStep","%d",robot.moveStep);
            telemetry.addData("Starting Arm Postition", "%d", startingArmPos);
            telemetry.addData("Found","%s",found);
            //telemetry.addData("Visible Target", visionSystem.getmineralPostion());
            //telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
            //        visionSystem.getRobotX(), visionSystem.getRobotY(), visionSystem.getRobotZ());
            telemetry.addData("armPosition", "%d ", robot.arm1.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("MCMD", "%.1f %.1f", motion.vD, motion.vTheta);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());

            telemetry.update();

        }
       // visionSystem.deactivateTFOD();
    }

}
