//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import java.security.KeyStore;
import java.util.TimerTask;
@Disabled
@Autonomous
public class Team10785AutoMarker extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public Vision visionSystem;
    public String found;
    public int target;
    public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float maxSpeed = .4f;

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        visionSystem = new Vision();

        visionSystem.initVuforia(hardwareMap);
        visionSystem.initTfod(hardwareMap);


        telemetry.addData("Stauts", "Initialized 1");
        telemetry.update();
        visionSystem.activateTFOD();

        while (!opModeIsActive() && !isStopRequested()) {
            visionSystem.updateMineralPostion();
            storedMineralposition=visionSystem.getmineralPostion();
            telemetry.addData("StoredFound", "%s", storedMineralposition);

            telemetry.addData("status", "waiting for a start command...");
            telemetry.update();
        }

        //waitForStart();


        robot.arm1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.arm2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        target = 0;

        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        //commented out TeleOp controls

        while (opModeIsActive()) {


            robot.arm1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.arm2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.arm1.setTargetPosition(target);
            robot.arm2.setTargetPosition(target);
            robot.arm1.setPower(.50);
            robot.arm2.setPower(.50);

            //visionSystem.updateMineralPostion();
            //if (gamepad1.a) {           // teleop
            switch (autonomousStep) {
                case 1:   // Land
                    //robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    //robot.lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                    //robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    //robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                    robot.lift.setPower(-1);
                    robot.lift2.setPower(1);
                    autonomousStep = 2;
                    break;


                case 2:
                    if (robot.lift.getCurrentPosition() <= -570 || robot.lift2.getCurrentPosition() <= -570) {         // Checks if lift is busy
                        robot.lift.setPower(0);
                        robot.lift2.setPower(0);
                        robot.movePower = -maxSpeed;
                        robot.moveTarget = -3.0f;
                        robot.move();
                        currentTime = robot.period.seconds();
                        autonomousStep = 300;
                    }break;


                case 200:
                    if (robot.moveStep == 3) {
                        //storedMineralposition = visionSystem.getmineralPostion();
                        if ((robot.period.seconds() - currentTime > 3)){
                            //Not Found go on
                            autonomousStep = 300;
                        }
                        if ((storedMineralposition == "Left" || storedMineralposition == "Center" || storedMineralposition == "Right")) {
                            autonomousStep = 300;
                        }
                    }break;


                case 300:
                    if (robot.moveStep == 3 || robot.moveStep == 0) {

                        robot.movePower = 0.4f;
                        robot.moveTarget = 19.0f;
                        robot.strafe();
                        autonomousStep = 3;
                    }break;


                    //-------------- Start Mineral Check -------------------
                case 3:
                    //storedMineralposition = visionSystem.getmineralPostion();
                    if ((robot.moveStep == 3 ) ) {
                        if (storedMineralposition == "Left") {
                            robot.movePower = -0.4f;
                            robot.moveTarget = -6.0f;
                            robot.move();
                            autonomousStep = 4;


                        } else if (storedMineralposition == "Right") {
                            robot.movePower = 0.4f;
                            robot.moveTarget = 24.0f;
                            robot.move();
                            autonomousStep = 5;


                        } else {
                                robot.movePower = 0.4f;
                                robot.moveTarget = 10.0f;
                                robot.move();
                                autonomousStep = 6;
                        }
                    }break;
                    //----------------- End Mineral Check --------------------






                    //----------------- Moves Toward Mineral -----------------

                // LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT
                case 4:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.4f;
                        robot.moveTarget = 39.0f;
                        robot.strafe();
                        autonomousStep = 700;
                    }break;


                case 700:
                    if (robot.moveStep == 3) {
                        robot.movePower = maxSpeed;
                        robot.moveTarget = robot.inchesPer360 * 133 / 360;
                        robot.moveStep = 4;
                        autonomousStep = 777;
                    }break;
                case 777:
                    if (robot.moveStep == 3) {
                        robot.movePower = -maxSpeed;
                        robot.moveTarget = -17.0f;
                        robot.move();
                        autonomousStep = 800;
                    }break;

                case 800:
                    if (robot.moveStep == 3) {
                        startTime = runtime.seconds();
                        robot.collector1.setPower(-1);
                        robot.collector2.setPower(1);
                        autonomousStep = 810;
                    }break;


                case 810:
                    if (runtime.seconds() - startTime > 2) {
                        robot.collector1.setPower(0);
                        robot.collector2.setPower(0);
                        robot.movePower = -0.4f;
                        robot.moveTarget = -5.0f;
                        robot.strafe();
                        autonomousStep = 888;
                        }break;
                case 888:
                    if (robot.moveStep == 3) {
                        robot.movePower = maxSpeed;
                        robot.moveTarget = 72.0f;
                        robot.move();
                        target = 4665;
                        startTime = runtime.seconds();
                        robot.extend1.setPower(1);
                        robot.extend2.setPower(-1);
                        autonomousStep = 900;
                    }break;
                case 900:
                    if (runtime.seconds() - startTime > 14) {
                        robot.extend1.setPower(0);
                        robot.extend2.setPower(0);
                    }
                    break;




                    // Right Right Right Right Right Right Right Right Right Right Right Right Right
                case 5:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.4f;
                        robot.moveTarget = 39.0f;
                        robot.strafe();
                        autonomousStep = 111;
                    }break;
                case 111:
                    if (robot.moveStep == 3) {
                        robot.movePower = -maxSpeed;
                        robot.moveTarget = -28;
                        robot.move();
                        autonomousStep = 700;
                    }break;




                    // Center Center Center Center Center Center Center Center Center Center Center
                case 6:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.4f;
                        robot.moveTarget = 39.0f;
                        robot.strafe();
                        autonomousStep = 121;
                    }break;
                case 121:
                    if (robot.moveStep == 3) {
                        robot.movePower = -maxSpeed;
                        robot.moveTarget = -16.0f;
                        robot.move();
                        autonomousStep = 700;
                    }break;

            }
            //---------------- Ejected Mineral -------------------

            robot.moveUpdate();
            telemetry.addData("Status", "Running");
            telemetry.addData("MoveStep", "%d", robot.moveStep);
            telemetry.addData("Found", "%s", found);
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
                    visionSystem.getRobotX(), visionSystem.getRobotY(), visionSystem.getRobotZ());
            telemetry.addData("armPosition", "%d ", robot.arm1.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("AutonomousStep", "%d", autonomousStep);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());

            telemetry.update();

        }
        visionSystem.deactivateTFOD();
    }

}//}
