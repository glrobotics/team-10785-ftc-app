//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import java.security.KeyStore;
import java.util.TimerTask;

@Autonomous
public class Team10785AutoMarker2 extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public Vision visionSystem;
    public String found;
    public int target;
    public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float maxSpeed = .4f;

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        visionSystem = new Vision();

        visionSystem.initVuforia(hardwareMap);
        visionSystem.initTfod(hardwareMap);


        telemetry.addData("Stauts", "Initialized 1");
        telemetry.update();
        visionSystem.activateTFOD();

        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        while (!opModeIsActive() && !isStopRequested()) {
            visionSystem.updateMineralPostion();
            storedMineralposition=visionSystem.getmineralPostion();
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());


            telemetry.addData("status", "waiting for a start command...");
            telemetry.update();
        }

        //waitForStart();


        robot.arm1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.arm2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        target = 0;


        //commented out TeleOp controls

        while (opModeIsActive()) {


            robot.arm1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.arm2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.arm1.setTargetPosition(target);
            robot.arm2.setTargetPosition(target);
            robot.arm1.setPower(.50);
            robot.arm2.setPower(.50);

            //visionSystem.updateMineralPostion();
            //if (gamepad1.a) {           // teleop
            switch (autonomousStep) {
                case 1:   // Land
                    //robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    //robot.lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                    //robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    //robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                    robot.lift.setPower(-1);
                    robot.lift2.setPower(1);
                    autonomousStep = 2;
                    break;


                case 2:
                    if (robot.lift.getCurrentPosition() <= -560 || robot.lift2.getCurrentPosition() <= -560) {         // Checks if lift is busy
                        robot.lift.setPower(0);
                        robot.lift2.setPower(0);
                        robot.movePower = -maxSpeed;
                        robot.moveTarget = -3.0f;
                        robot.moveStep = 1;
                        currentTime = robot.period.seconds();
                        autonomousStep = 300;
                    }break;


                case 200:
                    if (robot.moveStep == 3) {
                        //storedMineralposition = visionSystem.getmineralPostion();
                        if ((robot.period.seconds() - currentTime > 3)){
                            //Not Found go on
                            autonomousStep = 300;
                        }
                        if ((storedMineralposition == "Left" || storedMineralposition == "Center" || storedMineralposition == "Right")) {
                            autonomousStep = 300;
                        }
                    }break;


                case 300:
                    if (robot.moveStep == 3 || robot.moveStep == 0) {

                        robot.movePower = 0.4f;
                        robot.moveTarget = 19.0f;
                        robot.moveStep = 6;
                        autonomousStep = 301;
                    }break;
                // spinny thing here
                case 301:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.1f;
                        robot.moveTarget = 0.0f;
                        robot.moveStep = 10;
                        autonomousStep = 3;
                    }break;


                //-------------- Start Mineral Check -------------------
                case 3:
                    //storedMineralposition = visionSystem.getmineralPostion();
                    if ((robot.moveStep == 3 ) ) {
                        if (storedMineralposition == "Left") {
                            robot.movePower = -0.4f;
                            robot.moveTarget = -4.0f;
                            robot.moveStep = 1;
                            autonomousStep = 4;


                        } else if (storedMineralposition == "Right") {
                            robot.movePower = 0.4f;
                            robot.moveTarget = 24.0f;
                            robot.moveStep = 1;
                            autonomousStep = 5;


                        } else {
                            robot.movePower = 0.4f;
                            robot.moveTarget = 4.5f;
                            robot.moveStep = 1;
                            autonomousStep = 6;
                        }
                    }break;
                //----------------- End Mineral Check --------------------






                //----------------- Moves Toward Mineral -----------------

                // LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT LEFT
                case 4:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.4f;
                        robot.moveTarget = 14f;
                        robot.moveStep = 6;
                        autonomousStep = 41;
                    }break;

                case 41:
                    if (robot.moveStep == 3) {
                        robot.movePower = -0.4f;
                        robot.moveTarget = -12.5f;
                        robot.moveStep = 6;
                        autonomousStep = 700;
                    }break;
                //correct the degree


                case 4141:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.1f;
                        robot.moveTarget = 0.0f;
                        robot.moveStep = 10;
                        autonomousStep = 700;
                    }break;
                case 700:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.1f;
                        robot.moveTarget = -8.0f;
                        robot.moveStep = 10;
                        autonomousStep = 7123;
                    }break;
                case 7123: //move to wall
                    if (robot.moveStep == 3) {
                        robot.movePower = -.75f;
                        robot.moveTarget = -20f;
                        robot.moveStep = 1;
                        autonomousStep = 777;
                    }break;

                case 777: //turn to 45
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.45f;
                        robot.moveTarget = -45-90+16;
                        robot.moveStep = 10;
                        autonomousStep = 6767;
                    }break;
                case 6767: //strafe into wall
                    if (robot.moveStep == 3) {
                        robot.movePower = -0.5f;
                        robot.moveTarget = -5.0f;
                        robot.moveStep = 6;
                        autonomousStep = 6868;
                    }break;
                case 6868: //strafe away from wall
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.5f;
                        robot.moveTarget = 1.0f;
                        robot.moveStep = 6;
                        autonomousStep = 7777;
                    }break;
//                case 810:
//                    if (runtime.seconds() - startTime > 2) {
//                        robot.collector1.setPower(0);
//                        robot.collector2.setPower(0);
//                        robot.movePower = 0.4f;
//                        robot.moveTarget = 5.0f;
//                        robot.moveStep = 6;
//                        autonomousStep = 7777;
//                    }break;
//straight
                case 7777: //move to depot
                    if (robot.moveStep == 3) {
                        robot.movePower = -1;
                        robot.moveTarget = -40.5f;
                        robot.moveStep = 1;
                        autonomousStep = 800;
                    }break;
                case 800: //collector
                    if (robot.moveStep == 3) {
                        startTime = runtime.seconds();
                        robot.collector1.setPower(-1);
                        robot.collector2.setPower(1);
                        autonomousStep = 7878;
                    }break;
                case 7878:
                    if (runtime.seconds()-startTime > 1.75 ){
                        robot.movePower = maxSpeed;
                        robot.moveTarget = 55f;
                        robot.moveStep = 1;
                        target = 4665;
                        startTime = runtime.seconds();
                        robot.extend1.setPower(1);
                        robot.extend2.setPower(-1);
                        robot.collector1.setPower(0);
                        robot.collector2.setPower(0);
                        autonomousStep = 900;
                    }break;


                //staight
                case 900:
                    if (runtime.seconds() - startTime > 14) {
                        robot.extend1.setPower(0);
                        robot.extend2.setPower(0);
                    }
                    break;




                // Right Right Right Right Right Right Right Right Right Right Right Right Right
                case 5:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.4f;
                        robot.moveTarget = 11f;
                        robot.moveStep = 6;
                        autonomousStep = 51;
                    }break;
                case 51:
                    if (robot.moveStep == 3) {
                        robot.movePower = -0.4f;
                        robot.moveTarget = -11f;
                        robot.moveStep = 6;
                        autonomousStep = 5151;
                    }break;
                //correct the degree
                case 5151:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.1f;
                        robot.moveTarget = 0f;
                        robot.moveStep = 10;
                        autonomousStep = 111;
                    }break;
                case 111:
                    if (robot.moveStep == 3) {
                        robot.movePower = -.7f;
                        robot.moveTarget = -34.5f;
                        robot.moveStep = 1;
                        autonomousStep = 700;
                    }break;




                // Center Center Center Center Center Center Center Center Center Center Center
                case 6:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.4f;
                        robot.moveTarget = 9.5f;
                        robot.moveStep = 6;
                        autonomousStep = 61;
                    }break;

                case 61:
                    if (robot.moveStep == 3) {
                        robot.movePower = -0.7f;
                        robot.moveTarget = -8.5f;
                        robot.moveStep = 6;
                        autonomousStep = 6161;
                    }break;
                //correct the degree
                case 6161:
                    if (robot.moveStep == 3) {
                        robot.movePower = 0.1f;
                        robot.moveTarget = 0f;
                        robot.moveStep = 10;
                        autonomousStep = 121;
                    }break;

                case 121:
                    if (robot.moveStep == 3) {
                        robot.movePower = -0.8f;
                        robot.moveTarget = -13f;
                        robot.moveStep = 1;
                        autonomousStep = 700;
                    }break;

            }
            //---------------- Ejected Mineral -------------------

            robot.moveUpdate();
            telemetry.addData("Status", "Running");
            telemetry.addData("MoveStep", "%d", robot.moveStep);
            telemetry.addData("Found", "%s", found);
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            //telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
            //        visionSystem.getRobotX(), visionSystem.getRobotY(), visionSystem.getRobotZ());
            telemetry.addData("armPosition", "%d ", robot.arm1.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("AutonomousStep", "%d", autonomousStep);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.update();

        }
        visionSystem.deactivateTFOD();
    }

}//}
