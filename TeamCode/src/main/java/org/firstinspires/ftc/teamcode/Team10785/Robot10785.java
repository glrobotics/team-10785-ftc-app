package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.sun.tools.javac.tree.DCTree;
import com.qualcomm.hardware.bosch.BNO055IMU;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import java.util.Locale;

public class Robot10785 {
    public CRServo collector1;
    public CRServo collector2;
    public CRServo extend2;
    public CRServo extend1;
    public DcMotor lift;
    public DcMotor lift2;
    public DcMotor arm1;
    public DcMotor arm2;
    public Servo teammarker;
    public DcMotor leftFrontMotor;
    public DcMotor rightFrontMotor;
    public DcMotor leftBackMotor;
    public DcMotor rightBackMotor;
    public int moveStep = 0;
    public float movePower = 0;
    public float moveTarget = 0;
    public  int countPerInch = 119;
    public float inchesPer360 = 79.0116f;

    public BNO055IMU imu;

    public Orientation angles;
    public Acceleration gravity;


    /* local OpMode members. */
    HardwareMap hwMap           =  null;
    public ElapsedTime period  = new ElapsedTime();


    public void Robot10785() {
    }

    //Chances are this is all you're here for:\\
    public void turn() {
        moveStep = 10;
    }
    public void move(){
        moveStep = 1;
    }
    public void strafe(){
        moveStep = 6;
    }
    //end of all you're here for\\

    public void setrobotradius(double rad){
        inchesPer360 = (float)(rad * 2 * Math.PI);
    }
    public void init(HardwareMap ahwMap){

        hwMap = ahwMap;

        lift=hwMap.get(DcMotor.class, "lift");
        lift2=hwMap.get(DcMotor.class, "lift2");
        arm1=hwMap.get(DcMotor.class, "arm1");
        arm2=hwMap.get(DcMotor.class, "arm2");
        teammarker=hwMap.get(Servo.class, "teammarker");
        collector1=hwMap.get(CRServo.class, "collector1");
        collector2=hwMap.get(CRServo.class, "collector2");
        extend1=hwMap.get(CRServo.class, "extend1");
        extend2=hwMap.get(CRServo.class, "extend2");
        leftFrontMotor=hwMap.get(DcMotor.class, "lfWheel");
        leftBackMotor=hwMap.get(DcMotor.class, "lrWheel");
        rightFrontMotor=hwMap.get(DcMotor.class, "rfWheel");
        rightBackMotor=hwMap.get(DcMotor.class, "rrWheel");

        arm1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        arm2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        //lift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //lift2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lift.setPower(0);
        lift2.setPower(0);


        leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        leftFrontMotor.setDirection(DcMotor.Direction.FORWARD);
        leftBackMotor.setDirection(DcMotor.Direction.FORWARD);
        rightFrontMotor.setDirection(DcMotor.Direction.REVERSE);
        rightBackMotor.setDirection(DcMotor.Direction.REVERSE);
        arm1.setDirection(DcMotor.Direction.REVERSE);
        arm2.setDirection(DcMotor.Direction.FORWARD);

        leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        collector1.setPower(0);
        collector2.setPower(0);
        extend1.setPower(0);
        extend2.setPower(0);

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit            = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit            = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.loggingEnabled       = true;
        parameters.useExternalCrystal   = true;
        parameters.mode                 = BNO055IMU.SensorMode.IMU;
        parameters.loggingTag           = "IMU";
        imu                             = hwMap.get(BNO055IMU.class, "imu");

        imu.initialize(parameters);
    }
    public float getRobotAngle() {
        angles  = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        return (angles.firstAngle);
    }

public void moveUpdate () {
        switch (moveStep) {
            case 0:
                break; //End everything

                //MOVE THE ROBOT
            case 1:
                leftFrontMotor.setPower(movePower);
                leftBackMotor.setPower(movePower);
                rightFrontMotor.setPower(movePower);
                rightBackMotor.setPower(movePower);
                moveStep = 11; //this and the break statement can be removed to no effect
                break;
            case 11:
                if (Math.abs(moveTarget*countPerInch * .1) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(movePower);
                    leftBackMotor.setPower(movePower);
                    rightFrontMotor.setPower(movePower);
                    rightBackMotor.setPower(movePower);
                    moveStep = 12; //this and the break statement can be removed to no effect
                }
                break;
            case 12:
                if (Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(movePower);
                    leftBackMotor.setPower(movePower);
                    rightFrontMotor.setPower(movePower);
                    rightBackMotor.setPower(movePower);
                    moveStep = 2;
                }
                break;
            case 2:
                if (Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = 3;
                }
                break;
            //END OF MOVING THE ROBOT

            case 3:
                moveStep = 0;
                break;

                //DEPRECIATED TURNING ALGORITHM
             case 4:
                leftFrontMotor.setPower(movePower);
                leftBackMotor.setPower(movePower);
                rightFrontMotor.setPower(-movePower);
                rightBackMotor.setPower(-movePower);
                moveStep = 5;
                break;
            case 5:
                if (Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = 3;
                }
                break;
            //END OF DEPRECIATED TURNING ALGORITHM


            //STRAFE
            case 6:
                leftFrontMotor.setPower(-movePower);
                leftBackMotor.setPower(movePower);
                rightFrontMotor.setPower(-movePower);
                rightBackMotor.setPower(movePower);
                moveStep = 9;
                break;
            //END OF STRAFE

//AAAAND IIIIIIIIIIII -HOLY SHIT- WILL ALWAYS LOOOVE- HOLYSHITWHATTHEFUCKDOESANYOFTHISEVENMEAN
            case 7:
                if (Math.abs(moveTarget*countPerInch * .1) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(movePower);
                    leftBackMotor.setPower(movePower);
                    rightFrontMotor.setPower(movePower);
                    rightBackMotor.setPower(movePower);
                    moveStep = 12;
                }
                break;
            case 8:
                if (Math.abs(moveTarget*countPerInch * .9) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(movePower * .2);
                    leftBackMotor.setPower(movePower * .2);
                    rightFrontMotor.setPower(movePower * .2);
                    rightBackMotor.setPower(movePower * .2);
                    moveStep = 2;
                }
                break;
            case 9:
                if (Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = 3;

                }
                break;

                //TURNS THE ROBOT
            case 10:
                if(moveTarget-getRobotAngle() < 0){
                    leftFrontMotor.setPower(movePower);
                    leftBackMotor.setPower(movePower);
                    rightFrontMotor.setPower(-movePower);
                    rightBackMotor.setPower(-movePower);
                    moveStep = 20;
                }
                else{
                    leftFrontMotor.setPower(-movePower);
                    leftBackMotor.setPower(-movePower);
                    rightFrontMotor.setPower(movePower);
                    rightBackMotor.setPower(movePower);
                    moveStep = 21;
                }
                break;

            case 20:
                if (moveTarget - getRobotAngle()  >= -2) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = 3;
                }
                break;

            case 21:
                if (moveTarget - getRobotAngle() <= 2) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = 3;
                }
                break;
            //END OF TURNING THE ROBOT
            /*case 6:
                arm1.setPower(movePower);
                arm2.setPower(movePower);
                moveStep = 7;
                break;
            case 7:
                arm1.setPower(0);
                arm2.setPower(0);
                arm1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                arm2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                arm1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                arm2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                moveStep = 3;
                break;

                */
            default:
                moveStep = 0;
                break;
        }
}

}
